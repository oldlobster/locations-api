<?php

namespace App\Classes\Locations\ApiClient;

use App\Exception\GeocodesException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;
use Psr\Http\Message\ResponseInterface;
use Illuminate\Support\Facades\Log;
use App\Classes\Locations\Utils;

abstract class ApiClient
{
    protected $client;

    /**
     * The API key
     *
     * @var string
     */
    protected $apiKey;

    /**
     * The initial API URL
     *
     * @var string
     */
    protected $apiUrl;

    /**
     * The final API URL with query appended
     *
     * @var string
     */
    protected $effectiveUrl;

    /**
     * Create a new api client instance
     *
     * @param  \GuzzleHttp\Client  $client
     * @param  string  $apiKey
     * @return void
     */
    public function __construct(Client $client, $apiKey)
    {
        $this->client = $client;
        $this->apiKey = $apiKey;
    }

    /**
     * Perform a GET request
     *
     * @param string  $apiUrl
     * @param RequestContract $requestContract
     *
     * @return array
     */
    public function get($apiUrl, $query = [])
    {
        $response = [];

        if (!empty($this->client) && !empty($apiUrl)) {
            $request = [
                'http_error' => false,
                'headers' => [
                    'Content-Type' => 'application/json',
                ]
            ];

            // Append the API key to the query
            if (!empty($this->apiKey)) {
                $query['key'] = $this->apiKey;
            }

            try {
                // Build the query from the query array
                $queryString = http_build_query($query);
                $endpoint = $apiUrl . '?' . $queryString;

                // The final URL that is to be used
                $this->effectiveUrl = $endpoint;

                // Log request URL is helpful for monitoring
                Utils::log('info', 'Using effective Url', [$endpoint]);

                // Do the actual GET request
                $response = $this->client->request(
                    'GET',
                    $endpoint,
                    $request
                );
            } catch (RequestException $e) {
                $response = $e->getResponse();
                Utils::log(
                    'error',
                    'ApiClient RequestException has occurred',
                    [$apiUrl]
                );
                Utils::log(
                    'debug',
                    'RequestException',
                    [$e->getResponse]
                );
            }
        }

        if (!empty($response)) {
            $response =  $this->parseResponse($response);
        }
        return $response;
    }

    /**
     * Checks a string and returns if its valid JSON
     *
     * @param string $string
     *
     * @return bool
     */
    public function isValidJson($string)
    {
        if (is_string($string)) {
            json_decode($string);
            return (json_last_error() === JSON_ERROR_NONE);
        }

        return false;
    }

    /**
     * Parses the response and returns it as an array
     *
     * @param \Psr\Http\Message\ResponseInterface $response
     *
     * @return array
     */
    public function parseResponse(ResponseInterface $response)
    {
        $body = $response->getBody()->getContents();
        $validJson = ($this->isValidJson($body));

        if ($validJson) {
            $body = json_decode($body, true);
        }

        return [
            'code' => $response->getStatusCode(),
            'headers' => $response->getHeaders(),
            'body' => $body
        ];
    }
}
