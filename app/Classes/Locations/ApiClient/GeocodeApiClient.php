<?php

namespace App\Classes\Locations\ApiClient;

use App\Classes\Locations\ApiClient\ApiClient;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use App\Classes\Locations\Utils;

class GeocodeApiClient extends ApiClient
{
    /**
     * Constant representing a successful response
     *
     * @var string
     */
    const STATUS_OK = 'OK';

    /**
     * Constant representing a zero results response
     *
     * @var string
     */
    const STATUS_ZERO_RESULTS = 'ZERO_RESULTS';

    /**
     * Constant representing an over query limit response
     *
     * @var string
     */
    const STATUS_OVER_QUERY_LIMIT = 'OVER_QUERY_LIMIT';

    /**
     * Constant representing a request denied response
     *
     * @var string
     */
    const STATUS_REQUEST_DENIED = 'REQUEST_DENIED';

    /**
     * Constant representing an invalid request response
     *
     * @var string
     */
    const STATUS_INVALID_REQUEST = 'INVALID_REQUEST';

    /**
     * Constant representing an unknown errir response
     *
     * @var string
     */
    const STATUS_UNKNOWN_ERROR = 'UNKNOWN_ERROR';

    /**
     * Constant representing an empty address response
     *
     * @var string
     */
    const STATUS_EMPTY_ADDRESS = 'EMPTY_ADDRESS';

    /**
     * HTTP status code
     *
     * @var int
     */
    protected $statusCode;

    /**
     * Response status text
     *
     * @var string
     */
    protected $statusText;

    /**
     * Response content
     *
     * @var array
     */
    protected $response;

    /**
     * Create a geocode api client instance
     *
     * @param  \GuzzleHttp\Client  $client
     * @param  string  $apiKey
     * @param  string  $apiUrl
     * @return void
     */
    public function __construct(Client $client, $apiKey = null, $apiUrl = null)
    {
        $this->apiKey = $apiKey;
        $this->apiUrl = $apiUrl;

        parent::__construct($client, $apiKey);
    }

    /**
     * @param $address
     *
     * @return mixed|null
     */
    public function geocode($address)
    {
        Utils::log('info', 'Geocoding with address', [$address]);
        $geocodedResponse = [];

        // No point querying API with blank address
        if (empty($address)) {
            $this->statusCode = 400;
            $this->statusText = self::STATUS_EMPTY_ADDRESS;
            return $geocodedResponse;
        }

        // Make the GET request
        $responseArray = $this->get($this->apiUrl, ['address' => $address]);
        $this->response = $responseArray;

        // Process the response
        if (!empty($this->response)) {
            if (isset($this->response['code'])) {
                $this->statusCode = $this->response['code'];
            }
            if (isset($this->response['body']['status'])) {
                $this->statusText = $this->response['body']['status'];
            }
            if (isset($this->response['body']['results'])) {
                $results = $this->response['body']['results'];
                if (!empty($results)) {
                    // If theres multiple results,
                    // assume the first result is most relavant
                    $result = $results[0];

                    // Only need the formatted_address, lat, lng field.
                    // So we need to extract those only
                    if (isset($result['formatted_address'])) {
                        $formattedAddress = $result['formatted_address'];
                        $geocodedResponse['formatted_address'] =  $formattedAddress;
                    }
                    if (isset($result['geometry']['location'])) {
                        if (isset($result['geometry']['location']['lat'])) {
                            $geocodedResponse['lat'] = $result['geometry']['location']['lat'];
                        }
                        if (isset($result['geometry']['location']['lng'])) {
                            $geocodedResponse['lng'] = $result['geometry']['location']['lng'];
                        }
                    }
                }
            }
        }

        Utils::log('info', 'Geocoding response', [
            $this->statusCode,
            $this->statusText
        ]);

        if ($this->isSuccess()) {
            Utils::log('info', 'Geocoding Success', [$geocodedResponse]);
        } else {
            Utils::log('info', 'Geocoding Failed', [$this->response]);
        }

        return $geocodedResponse;
    }

    /**
     * Get the status code
     *
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Get the status text
     *
     * @return string
     */
    public function getStatusText()
    {
        return $this->statusText;
    }

    /**
     * Get the response
     *
     * @return array
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Get the API Url
     *
     * @return string
     */
    public function getApiUrl()
    {
        return $this->apiUrl;
    }

    /**
     * Set the API URl
     *
     * @param string  $apiURl
     * @return void
     */
    public function setApiUrl(string $apiUrl)
    {
        $this->apiUrl = $apiUrl;
    }

    /**
     * Determine whether the request was successful
     *
     * @return bool
     */
    public function isSuccess()
    {
        return $this->statusCode == 200 && $this->statusText == self::STATUS_OK;
    }
}
