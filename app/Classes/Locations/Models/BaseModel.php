<?php

namespace App\Classes\Locations\Models;

use App\Classes\Locations\Models\Model;
use App\Classes\Locations\Models\ValidatorInvalidArgumentException;
use Illuminate\Validation\ValidationException;

abstract class BaseModel extends Model
{
    /**
     * Rules for validation
     *
     * @var array
     */
    public $rules = [];

    /**
     * Messages
     *
     * @var array
     */
    public $messages = [];

    /**
     * Validate
     *
     * @var bool
     */
    protected $validate;

    /**
     * Validator instance
     *
     * @var \Illuminate\Validation\Validator
     */
    protected $validator;

    /**
     * Create a new model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->validator = app()->make('validator');
        $this->validate  = true;
        if ($this->validate) {
            $this->validate();
        }
    }

    /**
     * Validate
     *
     * @return null|App\Classes\Locations\Models\ValidationHttpException
     */
    public function validate()
    {
        $validator = $this->validator->make($this->attributes, $this->rules, $this->messages);
        if ($validator->fails()) {
            $exception = new ValidatorInvalidArgumentException();
            $exception
            ->setMessage('This is invalid')
            ->setErrors($validator->errors())
            ->setInput($this->attributes);

            throw $exception;
        }
    }
}
