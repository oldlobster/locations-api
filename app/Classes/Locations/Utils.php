<?php

namespace App\Classes\Locations;

use Illuminate\Support\Facades\Log;

class Utils
{
    /**
     * Log with exception handler - needed for unit testing
     *
     * @return void
     */
    public static function log(string $level, string $message, array $debug = [])
    {
        $levels = ['info', 'error', 'debug'];

        if (!in_array($level, $levels)) {
            $level = 'info';
        }

        try {
            Log::$level($message, $debug);
        } catch (\RuntimeException $e) {
            //
        } catch (\Exception $e) {
            //
        }
    }
}
