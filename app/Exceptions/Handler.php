<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use App\Classes\Locations\Models\ValidatorInvalidArgumentException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $jsonOnly = true;

        if ($jsonOnly || $request->wantsJson()) {
            return $this->renderExceptionAsJson($request, $exception);
        }

        return parent::render($request, $exception);
    }

    /**
     * Render an exception into a JSON response
     *
     * @param $request
     * @param Exception $exception
     * @return SymfonyResponse
     */
    protected function renderExceptionAsJson($request, Exception $exception)
    {
        // Currently converts AuthorizationException to 403 HttpException
        // and ModelNotFoundException to 404 NotFoundHttpException
        $exception = $this->prepareException($exception);

        // Default response
        $response = [
            'message' => 'Sorry, something went wrong.'
        ];

        $status = 500;

        // Build correct status codes and status texts
        switch ($exception) {
            case $exception instanceof ValidatorInvalidArgumentException:
                $status = 400;
                $response['message'] = 'invalids';
                $response['errors'] = $exception->getErrors();
                break;
            case $exception instanceof ValidationException:
                $status = 400;
                return $this->convertValidationExceptionToResponse($exception, $request);
            case $exception instanceof AuthenticationException:
                $status = 401;
                $response['message'] = $exception->getMessage();
                break;
            case $this->isHttpException($exception):
                $status = $exception->getStatusCode();
                $response['message'] = Response::$statusTexts[$status];
                break;
            default:
                break;
        }

        // Add debug info if app is in debug mode
        if (config('app.debug')) {
            // Add the exception class name, message and stack trace to response
            $response['debug']['exception'] = get_class($exception); // Reflection might be better here
            $response['debug']['message'] = $exception->getMessage();
            $response['debug']['trace'] = $exception->getTrace();
        }

        return response()->json($response, $status);
    }
}
