<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PropertyService;
use App\Repositories\PropertyRepositoryInterface;
use Illuminate\Support\Facades\Input;

class PropertyController extends Controller
{
    /**
     * Property repository
     *
     * @var \App\Repositories\PropertyRepositoryInterface
     */
    protected $propertyRepo;

    /**
     * Property repository
     *
     * @var \App\Services\PropertyService
     */
    protected $propertyService;

    /**
    * PropertyController constructor.
    *
    * @param  \App\Repositories\PropertyRepositoryInterface  $propertyRepo
    * @param  \App\Services\PropertyService  $propertyService
    * @return void
    */
    public function __construct(
        PropertyRepositoryInterface $propertyRepo,
        PropertyService $propertyService
    ) {
        $this->propertyRepo = $propertyRepo;
        $this->propertyService = $propertyService;
    }

    /**
     * Return the properties list as JSON
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Pagination param
        $page = Input::get('page_number', 1);

        // Cached param clears the property list cache
        $cached = Input::get('cached', 'true');

        $clearCache = $cached === 'false';
        $limit = 10;

        // Get the paged property list array
        $data = $this->propertyService->getPropertyList(
            $page,
            $limit,
            true,
            $clearCache
        );

        return response()->json($data, 200);
    }
}
