<?php

namespace App\Models;

use App\Classes\Locations\Models\BaseModel;
use App\Traits\Geocodable;

class Property extends BaseModel
{
    use Geocodable;

    /**
     * Hidden from response
     *
     * @var array
     */
    protected $hidden = [
        'address_line_1',
        'address_line_2',
        'postcode',
        'city'
    ];

    /**
     * Rules for validation
     *
     * @var array
     */
    public $rules = [
        'id'    => 'required|integer|min:1',
        'address_line_1' => 'required|string|min:1|max:500',
        'address_line_2' => 'string|min:1|max:500',
        'postcode' => 'string|min:1|max:10',
        'city' => 'string|min:1|max:250',
    ];

    /**
     * Create a new Property instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->original_address = $this->getAddressForQuery();
    }

    /**
     * Get formatted address fields
     *
     * @return string
     */
    public function getAddressForQuery()
    {
        $vals = [
            $this->address_line_1,
            $this->address_line_2,
            $this->city,
            $this->postcode
        ];

        $filtered = array_where($vals, function ($value, $key) {
            return is_string($value) && !empty($value);
        });

        return $address = implode(', ', $filtered);
    }

    /**
     * Callback when geocoding is successfully completed
     *
     * @return void
     */
    public function geocodingSuccess(array $geocoded)
    {
        if (isset($geocoded['lat']) && isset($geocoded['lng'])) {
            if (isset($geocoded['lat'])) {
                $this->lat = $geocoded['lat'];
            }

            if (isset($geocoded['lng'])) {
                $this->lng = $geocoded['lng'];
            }

            if (isset($geocoded['formatted_address'])) {
                $this->formatted_address = $geocoded['formatted_address'];
            }
        }
    }
}
