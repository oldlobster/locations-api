<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use GuzzleHttp\Client;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Classes\Locations\ApiClient\GeocodeApiClient',
            function ($app) {
                $client = new Client();

                $apiKey = env('GOOGLE_API_KEY');
                if (env('GOOGLE_GEOCODE_API_SANDBOX_MODE', true)) {
                    $apiUrl = env('GOOGLE_GEOCODE_API_SANDBOX_URL');
                } else {
                    $apiUrl = env('GOOGLE_GEOCODE_API_URL');
                }

                return new \App\Classes\Locations\ApiClient\GeocodeApiClient(
                    $client,
                    $apiKey,
                    $apiUrl
                );
            }
        );
    }
}
