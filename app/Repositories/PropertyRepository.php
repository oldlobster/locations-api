<?php

namespace App\Repositories;

use App\Models\Property;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use App\Classes\Locations\Models\ValidatorInvalidArgumentException;

class PropertyRepository implements PropertyRepositoryInterface
{
    /**
     * The path is used to retrieve data store
     *
     * @var string
     */
    protected $path;

    /**
     * The file is used to retrieve data store
     *
     * @var string
     */
    protected $file;

    /**
    * Create property repository instance
    *
    * @param PropertyRepositoryInterface $post
    */
    public function __construct()
    {
        // Remote or local file
        $this->source = env('PROPERTY_REPOSITORY_SOURCE');

        // Path for local file
        $this->path = Config::get('locations.paths.repository');

        // Filename for local file
        $this->file = 'properties.json';

        // Only used if loading data from remote URL
        $this->url = env('PROPERTY_REPOSITORY_URL');
    }

    /**
     * Get’s all properties.
     *
     * @return mixed
     */
    public function all()
    {
        $contents = $this->getSourceFile();
        if (!empty($contents)) {
            $contents = json_decode($contents, true);

            if (!empty($contents) && isset($contents['properties'])) {
                $contents = $this->toObjectArray($contents['properties']);
            }
        }

        return $contents;
    }

    /**
     * Gets the data file from local or remote source
     *
     * @return string
     */
    protected function getSourceFile()
    {
        $contents = [];

        if ($this->source === 'url') {
            $validator = Validator::make(['url' => $this->url], [
                'url' => 'required|url',
            ]);

            if (!$validator->fails()) {
                try {
                    $contents = file_get_contents($this->url);
                } catch (Exception $e) {
                    //
                }
            }
        } else {
            $dataFilePath = $this->path . DIRECTORY_SEPARATOR . $this->file;
            $contents = [];
            if (Storage::disk('local')->exists($dataFilePath)) {
                $contents = Storage::disk('local')->get($dataFilePath);
            }
        }

        return $contents;
    }

    /**
     * Get’s paginated properties
     *
     * @param  int  $page
     * @param  int  $limit
     * @return array
     */
    public function paginate($page = 1, $limit = 10)
    {
        $properties = $this->all();
        $offSet = ($page * $limit) - $limit;
        $itemsForCurrentPage = array_slice($properties, $offSet, $limit, true);

        $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
            $itemsForCurrentPage,
            count($properties),
            $limit,
            $page
        );

        return [
            'current_page' => $paginator->currentPage(),
            'total_pages' => $paginator->lastPage(),
            'result_count' => $paginator->total(),
            'data' => $paginator->items()
        ];
    }

    /**
     * Convert to array of Property objects.
     *
     * @param  array  $properties
     * @return array
     */
    protected function toObjectArray(array $properties)
    {
        $objects = [];
        if (!empty($properties)) {
            foreach ($properties as $property) {
                $propertyObj = $this->createOrContinue($property);
                if (!empty($propertyObj)) {
                    $objects[] = $propertyObj;
                }
            }
        }
        return $objects;
    }

     /**
     * Create without stopping for exceptions
     *
     * @param  array  $attributes
     * @return App\Models\Property
     */
    public function createOrContinue(array $attributes)
    {
        $return  = [];
        try {
            $return = new Property($attributes);
        } catch (ValidatorInvalidArgumentException $ie) {
            //
        } catch (Exception $ie) {
            //
        }

        return $return;
    }
}
