<?php

namespace App\Repositories;

interface PropertyRepositoryInterface
{
    /**
    * Get's all properties.
    *
    * @return mixed
    */
    public function all();
}
