<?php

namespace App\Services;

use App\Models\Property;
use App\Repositories\PropertyRepositoryInterface;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class PropertyService
{
    /**
     * @var \App\Repositories\PropertyRepositoryInterface
     */
    protected $propertyRepo;

    /**
     * InvoiceService constructor.
     *
     * @param ClientRepository  $clientRepo
     * @param InvoiceRepository $invoiceRepo
     * @param DatatableService  $datatableService
     */
    public function __construct(PropertyRepositoryInterface $propertyRepo)
    {
        $this->propertyRepo = $propertyRepo;
    }

    /**
     * Get the Properties list using cache strategy
     *
     * @param  int   $page
     * @param  int   $limit
     * @param  bool  $geocode
     * @param  bool  $clearCache
     * @return array
     */
    public function getPropertyList(
        $page = 1,
        $limit = 10,
        $geocode = true,
        $clearCache = false
    ) {
        // Env settings
        $cacheEnabled = env('PROPERTY_LIST_CACHE', false);
        $cacheTime = env('PROPERTY_LIST_CACHE_TIME', 1);

        $data = [];

        if ($cacheEnabled) {
            $cacheKey = 'properties_list_' . $page . '_' .$limit;

            // If request asks for non-cached version
            if ($clearCache) {
                Cache::forget($cacheKey);
            }

            // Get from cache or generate the cache for property list
            $data = Cache::remember(
                $cacheKey,
                $cacheTime,
                function () use ($page, $limit) {
                    $data = $this->getProperties(
                        $page,
                        $limit,
                        true
                    );

                    // Set this just for checking
                    $data['generated_at'] = Carbon::now()->toDateTimeString();
                    return $data;
                }
            );
        } else {
            $data = $this->getProperties(
                $page,
                $limit,
                true
            );

            $data['generated_at'] = Carbon::now()->toDateTimeString();
        }

        return $data;
    }

    /**
     * Get the Properties list
     *
     * @param  int   $page
     * @param  int   $limit
     * @param  bool  $geocode
     * @return array
     */
    public function getProperties($page = 1, $limit = 10, $geocode = true)
    {
        $geocodedProperties = [];
        $properties = $this->propertyRepo->paginate($page, $limit);

        foreach ($properties['data'] as $property) {
            $propertyArray = [];
            if ($geocode) {
                $propertyArray = $this->geocodeProperty($property);
            } else {
                $propertyArray = $property->toArray();
            }
            $geocodedProperties[] = $propertyArray;
        }

        $properties['data'] = $geocodedProperties;

        return $properties;
    }

    /**
     * Geocode the property
     *
     * @param  App\Models\Property  $property
     * @return array
     */
    public function geocodeProperty(Property $property)
    {
        // Caches the property object and geocoded data
        // Seperate from the property list cache
        $useCache = env('PROPERTY_OBJECT_CACHE', true);
        $cacheSecs = intval(env('PROPERTY_OBJECT_CACHE_TIME', 60));

        $propertyArray = [];
        if ($useCache) {
            // Generate unique cache key for the property
            $cacheKey = 'property_' . $property->id;
            if (Cache::has($cacheKey)) {
                // Attempt to load geocoded property array from the cache
                $propertyArray = Cache::get($cacheKey);
                $propertyArray['from_cache'] = true;
            } else {
                // Geocode if not found in cache
                $property->geocode();
                $propertyArray = $property->toArray();

                // Cache the geocoded property as an array
                Cache::put($cacheKey, $propertyArray, $cacheSecs);
                $propertyArray['from_cache'] = false;
            }
        } else {
            $property->geocode();
            $property['from_cache'] = false;
            $propertyArray = $property->toArray();
        }

        return $propertyArray;
    }
}
