<?php

namespace App\Traits;

use App\Classes\Locations\ApiClient\GeocodeApiClient;
use Illuminate\Support\Facades\Log;

trait Geocodable
{
    /**
     * Longitude
     *
     * @var float
     */
    protected $latitude;

    /**
     * Latitude
     *
     * @var float
     */
    protected $longitude;

    /**
     * Geocode using client API
     *
     * @return void
     */
    public function geocode()
    {
        $geocoder = app(GeocodeApiClient::class);
        //$address = '54 Westmount Rd, London SE9 1JE';
        $address = static::getAddressForQuery();

        $response = $geocoder->geocode($address);

        if ($geocoder->isSuccess()) {
            static::geocodingSuccess($response);
        }
    }

    /**
     * Get the formatted address for the query param
     *
     * @return string
     */
    abstract public function getAddressForQuery();

    abstract public function geocodingSuccess(array $geocoded);
}
