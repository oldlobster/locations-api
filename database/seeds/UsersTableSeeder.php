<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the user seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate our existing records to clear table
        User::truncate();

        $faker = \Faker\Factory::create();

        // Ensure everyone has the same password and
        // let's hash it before the loop, or else our seeder
        // will be too slow.
        $password = Hash::make('testapi123');
        $apiToken = 'mgldkNFzigP6gvwleoHlIyEqKGxRFWT2j5KFFNf1ntHMbWdpKMUGrolQIlOX';

        User::create([
            'name' => 'Administrator',
            'email' => 'admin@test.com',
            'password' => $password,
            'api_token' => $apiToken
        ]);

        // And now let's generate a few dozen users for our app:
        for ($i = 0; $i < 10; $i++) {
            User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => $password,
            ]);
        }
    }
}
