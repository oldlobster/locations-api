# Locations API

---

To setup on development:

1. Install Docker and Docker-Compose
2. Checkout the repository or Unzip the archive
	```git clone git@bitbucket.org:oldlobster/locations-api.git.```
3. Copy env file
	```cd locations-api```
	```cp .env.example .env```
4. Update .env file with production values for DB and APP (see ENV Settings below)
5. Build and run the docker instance
	```docker-compose up```
6. SSH into app container
	```docker-compose run app bash```
7. Run Composer install to get dependencies
	```composer install```
8. Set ownership of folder
	```chown -R www-data:www-data /var/www```
9. Generate laravel key by running this inside docker app server
	```php artisan key:generate```
10. Generate db tables (for user auth)
	```php artisan migrate```
	```php artisan db:seed```
Docker containers are now setup and running
11. Navigate to [http://localhost:8080/] in a browser to check its working

##Accessing the API

- The api is using an api token to authenticate, so access to /api routes are blocked by default
- After running db:seed a test user will be created that can be used to check the api routes

###Test User

email=admin@test.com

password=testapi123

api_token=mgldkNFzigP6gvwleoHlIyEqKGxRFWT2j5KFFNf1ntHMbWdpKMUGrolQIlOX

###Routes

GET /api/user

```
 curl -v GET http://localhost:8080/api/user \
 -H "Authorization: Bearer mgldkNFzigP6gvwleoHlIyEqKGxRFWT2j5KFFNf1ntHMbWdpKMUGrolQIlOX" \
 -H "Accept: application/json" \
 -H "Content-Type: application/json"
```

 GET /api/properties

```
 curl -X GET http://localhost:8080/api/properties \
 -H "Authorization: Bearer mgldkNFzigP6gvwleoHlIyEqKGxRFWT2j5KFFNf1ntHMbWdpKMUGrolQIlOX" \
 -H "Accept: application/json" \
 -H "Content-Type: application/json"
```

##Testing
The Unit and Feature tests can be run together from composer.  This can be done with the following command inside the docker container.
```composer test```

##ENV Settings

Here are some important settings in the .env file:

-----------------------
`PROPERTY_OBJECT_CACHE`

true|false

Sets whether each property is individually cached.  
This includes the geocode data from the Google API. So should be set to true otherwise will result in multiple duplicate requests to Google API.

-----------------------
`PROPERTY_OBJECT_CACHE_TIME`

int

Sets the time in seconds property objects will remain cached.

-----------------------
`PROPERTY_LIST_CACHE`

true|false

Sets whether the list itself is cached.  This is each paged set of properties.  

-----------------------

`PROPERTY_LIST_CACHE_TIME`

int

Sets the time in seconds property list pages will remain cached.

-----------------------
`PROPERTY_REPOSITORY_SOURCE`

file|url

This sets where the JSON data source is coming from can be a url or a local file

-----------------------
`PROPERTY_REPOSITORY_URL`

url

This allows a remote JSON file to be used.  Good for testing purposed as list will update automatically when the JSON file is changed.

-----------------------
`GOOGLE_API_KEY`

string

Use a key here to track api usage and quota

-----------------------
`GOOGLE_GEOCODE_API_SANDBOX_MODE`

true|false

This will use a dummy url for geocoding, that returns the same geocode.

-----------------------
`GOOGLE_GEOCODE_API_SANDBOX_URL`

url

This is for testing. Allows a dummy url to specified for geocoding.
e.g. https://api.myjson.com/bins/vekd3

-----------------------
`GOOGLE_GEOCODE_API_URL`

url

This is the real Google APi url for geocoding
e.g. https://maps.googleapis.com/maps/api/geocode/json
