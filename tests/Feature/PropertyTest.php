<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Models\Property;

class PropertyTest extends TestCase
{
    /**
     * Tests structure of properties list
     *
     * @return void
     */
    public function testPropertiesAreListedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        // GET the list of properties
        $response = $this->json('GET', '/api/properties', [], $headers);

        $response->assertStatus(200);

        // Check the structure is correct
        $response->assertJsonStructure([
            'current_page',
            'data' => [
                '*' => [
                    'id',
                    'lat',
                    'lng',
                    'formatted_address',
                    'from_cache',
                ]
            ]
        ]);
    }
}
