<?php

namespace Tests\Unit;

use App\Exception\GeocodesException;
use App\Classes\Locations\ApiClient\GeocodeApiClient;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Log;

class GeocodeApiClientTest extends TestCase
{
    /**
     * Geocode API Client instance
     *
     * @var string
     */
    protected $geocodeApi;

    /**
     * Handler for testing
     *
     * @var \GuzzleHttp\Handler\MockHandler
     */
    protected $mock;

     /**
     * Dir containing test data
     *
     * @var string
     */
    protected $mockDataDir;

    /**
     * Setup for the tests
     *
     * @return void
     */
    public function setUp()
    {
        $this->mockDataDir = __DIR__ . '/../data/Mock/Geocode/';

        // Use the Mock Handler for testing
        $this->mock = new MockHandler();
        $handler = HandlerStack::create($this->mock);
        $client = new Client(['handler' => $handler]);
        $this->geocodeApi = new GeocodeApiClient(
            $client,
            'test_api_key',
            'https://api.mock.com/api/geocode/json'
        );
    }

    /**
     * Teardown after tests
     *
     * @return void
     */
    public function tearDown()
    {
        $this->geocodeApi = null;
    }

    /**
     * Tests HTTP Response for invalid argument with empty address
     *
     * @return void
     */
    public function testShouldReturnInvalidRequestForEmptyAddressArgument()
    {
        $testJson = 'invalid_request.response.body.json';
        $expected = file_get_contents($this->mockDataDir . $testJson);

        $this->appendMockResponse(400, $expected);

        // Attempt to call with empty address
        $result = $this->geocodeApi->geocode('');

        // Check that response is 400
        $this->assertEquals(400, $this->geocodeApi->getStatusCode());

        // Check that status text is set appropriately
        $this->assertEquals(
            GeocodeApiClient::STATUS_EMPTY_ADDRESS,
            $this->geocodeApi->getStatusText()
        );

        // The response should be empty array
        $this->assertEquals([], $result);
    }

    /**
     * Tests HTTP Response for invalid address argument
     *
     * @return void
     */
    public function testShouldReturnZeroResultsForInvalidAddressArgument()
    {
        $testJson = 'zero_results.response.body.json';
        $expected = file_get_contents($this->mockDataDir . $testJson);

        $this->appendMockResponse(200, $expected);

        // Attempt to call with invalid address
        $result = $this->geocodeApi->geocode('!');

        // Check that response is 200
        $this->assertEquals(200, $this->geocodeApi->getStatusCode());

        // Check that status text is set appropriately
        $this->assertEquals(
            GeocodeApiClient::STATUS_ZERO_RESULTS,
            $this->geocodeApi->getStatusText()
        );

        // The response should be empty array
        $this->assertEquals([], $result);
    }

    /**
     * Tests HTTP Response for successfully returned geocode data
     *
     * @return void
     */
    public function testShouldReturnGeocodeData()
    {
        $testJson = 'success.se91hh.response.body.json';
        $expected = file_get_contents($this->mockDataDir . $testJson);

        $this->appendMockResponse(200, $expected);

        // Attempt to call with invalid address
        $result = $this->geocodeApi->geocode('se91hh');

        // Check that response is 200
        $this->assertEquals(200, $this->geocodeApi->getStatusCode());

        // Check that status text is set appropriately
        $this->assertEquals(
            GeocodeApiClient::STATUS_OK,
            $this->geocodeApi->getStatusText()
        );

        // Get the body of the response
        $response = $this->geocodeApi->getResponse();

        $this->assertArrayHasKey('body', $response);

        if (isset($response['body'])) {
            $responseBody = $response['body'];
            $expectedJson = json_decode($expected, true);
            $this->assertEquals($responseBody, $expectedJson);
        }
    }

    /**
     * Appends an expected response to the handler
     *
     * @param  int  $statusCode
     * @param  string  $body
     * @return void
     */
    protected function appendMockResponse($statusCode = 200, $body = '')
    {
        $this->mock->append(new Response($statusCode, [], $body));
    }
}
