<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Property;

class PropertyTest extends TestCase
{
    public function testConstructorFailsWithoutRequiredParameters()
    {
        $e = 'App\Classes\Locations\Models\ValidatorInvalidArgumentException';
        $this->expectException($e);

        $property = new Property([
            'city' => 'london',
            'address_line_3' => 'Lancashire'
        ]);
    }

    public function testConstructor()
    {
        $property = new Property([
            'id' => 100,
            'address_line_1' => '123 Fake St'
        ]);

        $this->assertEquals(100, $property->id);
        $this->assertEquals('123 Fake St', $property->address_line_1);
    }

    public function testHidden()
    {
        $property = new Property([
            'id' => 100,
            'address_line_1' => '123 Fake St',
            'postcode' => 'h13 ut5'
        ]);

        $attributes = $property->attributesToArray();
        $this->assertFalse(isset($attributes['postcode']));
        $this->assertContains('postcode', $property->getHidden());
    }

    public function testToArray()
    {
        $property = new Property([
            'id' => 100,
            'address_line_1' => '123 Fake St',
            'address_line_2' => 'High Street',
            'city' => 'london',
            'lat' => 54.232,
            'lng' => -3.00,
            'postcode' => 'h13 ut5',
            'formatted_address' => '123 Fake St, High Street, london, h13 ut5'
        ]);
        $array = $property->toArray();
        $this->assertTrue(is_array($array));
        $this->assertEquals(
            '123 Fake St, High Street, london, h13 ut5',
            $array['formatted_address']
        );
        $this->assertTrue(isset($array['lat']));
        $this->assertTrue(isset($array['lng']));
        $this->assertEquals($array, $property->jsonSerialize());
    }

    public function testGetAddressForQuery()
    {
        $testData = [
            'id' => 100,
            'address_line_1' => '123 Fake St',
            'address_line_2' => 'High Street',
            'city' => 'london',
            'lat' => 54.232,
            'lng' => -3.00,
            'postcode' => 'h13 ut5',
            'formatted_address' => '123 Fake St, High Street, london, h13 ut5'
        ];

        $property = new Property($testData);

        $correctAddressFormat = sprintf('%s, %s, %s, %s',
            $testData['address_line_1'],
            $testData['address_line_2'],
            $testData['city'],
            $testData['postcode']
        );

        $this->assertEquals($correctAddressFormat, $property->getAddressForQuery());
    }
}
